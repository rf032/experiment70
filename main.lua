-- EXPECIENCE 70
-- by Rennan

scenes = {
    splash = SplashScreen:create(),
    game = Game:create(),
    gameOver = GameOver:create(),
    win = Win:create()
}

reset = false
scene = "splash"

function _init()
    initialize()
end

function _update()
    get_scene().update()
end

function _draw()
    get_scene().draw()
end