GameOver = {}
GameOver.__index = GameOver

function GameOver:create()
   local gameOver = {}             
   setmetatable(gameOver,GameOver)  
   return gameOver
end

function GameOver:update()
    if (btn(5)) set_scene("game")
end

function GameOver:draw()
    cls()
    print('game over!', 37, 40, 8)  
    print('press x to restart', 32, 60, 7)  
end