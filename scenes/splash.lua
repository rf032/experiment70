SplashScreen = {}
SplashScreen.__index = SplashScreen

function SplashScreen:create()
   local splash = {}             
   setmetatable(splash,SplashScreen)  
   return splash
end

function SplashScreen:update()
    if (btn(5)) set_scene("game")
end

function SplashScreen:draw()
    cls()
    print('experiment 70', 37, 20)  

    print('collect them all \n before a minute!', 30, 35)

    lastPosition = print('collect all coins:', 32, 50)
    spr(35, lastPosition + 2, 48.5)

    lastPosition = print('collect all crystals:', 32, 60)
    spr(51, lastPosition + 2, 58.2)

    print('press x to start', 32, 90)  
end