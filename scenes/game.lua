Game = {}
Game.__index = Game
Game.seconds = 0
Game.minutes = 0
Game.fpsCount = 0
Game.timeMachimeSprite = 80
Game.coinsCount = 0
Game.crystalCount = 0
Game.treasures = { [1] = 35, [2] = 51, [3] = 52, [4] = 53 }

function Game:create()
   local game = {}             
   setmetatable(game,Game)  
   return Game
end

function Game:update()
    Game.fpsCount += 1
	control_player(pl)
	foreach(actor, move_actor)
end

function Game:draw()
    cls()
	room_x=flr(pl.x/16)
	room_y=flr(pl.y/16)
	camera(room_x*128,room_y*128)
	map()
	foreach(actor,draw_actor)
    Game.timer()
    Game.animations()
    Game.win()
    Game.gameOver()
    Game.hud()
end

function Game:timer()
    if (( Game.fpsCount % 30 ) == 0) then
        Game.seconds += 1
        if (Game.seconds == 60) then 
            Game.seconds = 0
            Game.minutes += 1
        end
    end
end

function Game:gameOver()
    if (Game.minutes >= 1) then
        Game.resetStatus()
        set_scene("gameOver")
    end
end

function Game:win()
    if(Game.coinsCount == 9 and Game.crystalCount == 1) then
        Game.resetStatus()
        --set_scene("win")
    end
end

function Game:hud()  
    timer = "timer:" .. Game.minutes .. ":" .. Game.seconds
    
    -- 1
    print(timer, 26, 122, 1)

    spr(35, 64, 120)
    print(Game.coinsCount, 72, 122, 1)

    spr(51, 81, 120)
    print(Game.crystalCount, 91, 122)

    -- 2
    print(timer, 26+128, 122, 1)

    spr(35, 64+128, 120)
    print(Game.coinsCount, 72+128, 122, 1)

    spr(51, 81+128, 120)
    print(Game.crystalCount, 91+128, 122)

    -- 3
    print(timer, 26+128, 122+128, 1)

    spr(35, 64+128, 120+128)
    print(Game.coinsCount, 72+128, 122+128, 1)

    spr(51, 81+128, 120+128)
    print(Game.crystalCount, 91+128, 122+128)
end

function Game:resetStatus()
    Game.seconds = 0
    Game.minutes = 0
    Game.coinsCount = 0
    Game.crystalCount = 0
    scenes["game"] = Game:create()
    initialize()
end

function Game:animations()
    if (Game.fpsCount % 10 == 0) then 
        Game.timeMachimeSprite += 2
        if (Game.timeMachimeSprite == 86) Game.timeMachimeSprite = 80
    end
    spr(Game.timeMachimeSprite,  25, 32, 2, 2)
end